'''
Способ решение задачи с собеседования. Описание задачи в корне проекта.
Blit является своего рода алгоритмом контрольной суммы, который принимает двоичное число
 и вычисляет один бит контрольной суммы.

Строки для проверки работы алгоритма:
1110 1111 0011 1000
1100 1001 0000 1111
1101 1010 1010 0010
0010 0001 0110 1000
1100 0010 0011 0100
1110 1111 0011 1000 1100 1001 0000 1111 1101 1010 1010 0010 0010 0001 0110 1000

word_check(word) - проверка на корректность ввода мтроки пользователем
matrix_create(len_array) - формируется начальная матрица индексов по длине слова
piramida_create (matrix, user_word) - создается матрица-шаблон, биты строки разбиваются для обработки
piramida_pattern(array_val) - преобразование значений по предложенным формулам
return_word(array_ind, array_val, len_user_word) - обратное преобразование для вывода пользователю
piramida_ease(array_val) - упрощение пирамиды, если все 1/0
blit(word) - основная ПП обработки
test() - проверка алгоритма на тестовых строках.
Строки для проверки работы алгоритма:
1110 1111 0011 1000
1100 1001 0000 1111
1101 1010 1010 0010
0010 0001 0110 1000
1100 0010 0011 0100
1110 1111 0011 1000 1100 1001 0000 1111 1101 1010 1010 0010 0010 0001 0110 1000

Произведена проверка времени работы с применением модулей timeit, cProfile

Вариант 1

'''

# проверка на корректность пользовательского ввода
def word_check(word):

    # проверка на буквы
    check = word.isdigit()
    if check:
        # проверка разрядности
        n, bit_depth = 1, 0
        while bit_depth < len(word):

            bit_depth = pow(4, n)
            n += 1

            if bit_depth > len(word):
                check = False
                break

    return check

# подготавливаем данные к обработки - формируем начальную матрицу индексов
def matrix_create(len_array):

    # кол элементов строки пирамиды
    array_count, index = 0, 0
    array, row = [], []

    # формируем матрицу индексов
    for item in range(len_array):
        row.append(item)
        index += 1

        if index > array_count:
            array.append(row)
            row = []
            index = 0
            array_count += 2

    return array

# эталонная матрица индексов
def piramida_create (matrix, user_word):

    array_ind = [[-1 for _ in range(4)] for _ in range(int(len(user_word) / 4))]
    array_val = [[-1 for _ in range(4)] for _ in range(int(len(user_word) / 4))]

    row_index = 0
    row_full = None

    # перебор по значениям
    for line in matrix:
        if row_full != None:
            row_index = row_full

        for index in line:
            array_item = int(format(index,'5b')[-2:],2)

            if (row_full != None) and (array_item < 2): row_index += 1
            array_ind[row_index][array_item] = index
            array_val[row_index][array_item] = user_word[index]

            # проверка строки массива на заполненность
            if not(array_ind[row_index].count(-1)):
                row_full = row_index

    val = [''.join(line) for line in array_val]

    return array_ind, val

# обработка матрицы значений по шаблонам
def piramida_pattern(array_val):
    # для удобства обработки, формат (последовательность бит) слова изменен 0-1-2-3 вместо 3-2-1-0
    patterns = {
    '0000': '0000',
    '1000': '0001',
    '0100': '1000',
    '1100': '0100',
    '0010': '0000',
    '1010': '0100',
    '0110': '1101',
    '1110': '1101',
    '0001': '0010',
    '1001': '1010',
    '0101': '1110',
    '1101': '1111',
    '0011': '1011',
    '1011': '0111',
    '0111': '1110',
    '1111': '1111'
    }
    checksum = 0

    #print(array_val)

    for i,item in enumerate(array_val):
        array_val[i] = patterns[item]
        if (array_val[i].count('0') == len(array_val[i])) or (array_val[i].count('1') == len(array_val[i])):
            checksum += 1

    return array_val, checksum

# обратное проебразование
def return_word(array_ind, array_val, len_user_word):
    array = [[item[i:(i + 1)] for i in range(len(item))] for item in array_val]
    user_word = [None for _ in range(len_user_word)]

    row, col = len(array_ind), len(array_ind[0])
    for j in range(row):
        for i in range(col):
            index = array_ind[j][i]
            user_word[index] = array_val[j][i]

    return array, ''.join(user_word)[::-1]

# упрощение матрицы
def piramida_ease(array_val):
    user_word = ''
    for item in array_val:
        user_word += item[0]
    return user_word[::-1]

# основная обрабатывающая функция
def blit(word):
    print(f'Проверка слова: {word}')
    while len(word) != 1:
        user_word = word[::-1]

        # получаем исходную матрицу
        matrix = matrix_create(len(user_word))

        # матрица-эталон, матрица значений
        array_ind, array_val = piramida_create(matrix,user_word)

        checksum = 0
        while checksum < len(array_val):
            # преобразование по шаблону
            array_val, checksum = piramida_pattern(array_val)

            # возвращаем преобразванное слово в пользовательском формате
            array_return, user_word_return = return_word(array_ind, array_val, len(user_word))
            print(user_word_return)

        # после упрощения
        word = piramida_ease(array_val)
        print(word)

    return word

# тест
def test():
    test_words = ['1110111100111000',
                  '1100100100001111',
                  '1101101010100010',
                  '0010000101101000',
                  '1100001000110100',
                  '1110111100111000110010010000111111011010101000100010000101101000'
                  ]
    for word in test_words:
        if word_check(word):
            print(f'Результат проверки слова {word}: {blit(word)}\n')
        else:
            print(f'Ошибка ввода слова {word}!\n')
            continue
    print('Проверка закончена')

if __name__ == '__main__':
    #word = input('Введите число для проверки:')
    #
    # проверка на корректность ввода значения -
    # if word_check(word):
    #     print(f'Результат проверки строки {word}: {blit(word)}')
    # else:
    #     print(f'Вы ввели некорректную строку: {word}. Исправьте, и повторите попытку.')

    # проверка на тестовых строках
    test()

    # проверка на время выполнения
    #import timeit
    #python -m timeit -n 10000 -s "import blit" "blit.blit('1110111100111000')"
    #1000 loops, best of 3: 1.24 msec per loop
    #5000 loops, best of 3: 1.19 msec per loop
    #10000 loops, best of 3: 1.16 msec per loop

    # python -m timeit -n 10000 -s "import blit" "blit.blit('1110111100111000110010010000111111011010101000100010000101101000')
    # 1000 loops, best of 3: 3.43 msec per loop
    # 5000 loops, best of 3: 4.16 msec per loop
    # 10000 loops, best of 3: 4.41 msec per loop

    # import cProfile
    # cProfile.run('blit("1110111100111000110010010000111111011010101000100010000101101000")')

    # 'blit("1110111100111000")'
    # 258 function calls in 0.001 seconds
    #
    #    Ordered by: standard name
    #
    #    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    #         1    0.000    0.000    0.001    0.001 <string>:1(<module>)
    #         7    0.000    0.000    0.001    0.000 blit.py:130(return_word)
    #         7    0.000    0.000    0.000    0.000 blit.py:131(<listcomp>)
    #         7    0.000    0.000    0.000    0.000 blit.py:132(<listcomp>)
    #         2    0.000    0.000    0.000    0.000 blit.py:143(piramida_ease)
    #         1    0.000    0.000    0.001    0.001 blit.py:150(blit)
    #         2    0.000    0.000    0.000    0.000 blit.py:49(matrix_create)
    #         2    0.000    0.000    0.000    0.000 blit.py:69(piramida_create)
    #         2    0.000    0.000    0.000    0.000 blit.py:71(<listcomp>)
    #         2    0.000    0.000    0.000    0.000 blit.py:72(<listcomp>)
    #         2    0.000    0.000    0.000    0.000 blit.py:93(<listcomp>)
    #         7    0.000    0.000    0.000    0.000 blit.py:98(piramida_pattern)
    #         1    0.000    0.000    0.001    0.001 {built-in method builtins.exec}
    #        20    0.000    0.000    0.000    0.000 {built-in method builtins.format}
    #        92    0.000    0.000    0.000    0.000 {built-in method builtins.len}
    #        10    0.000    0.000    0.000    0.000 {built-in method builtins.print}
    #        26    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
    #        20    0.000    0.000    0.000    0.000 {method 'count' of 'list' objects}
    #        34    0.000    0.000    0.000    0.000 {method 'count' of 'str' objects}
    #         1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    #        12    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}

    # 'blit("1110111100111000110010010000111111011010101000100010000101101000")'
    #  1024 function calls in 0.002 seconds
    #
    #    Ordered by: standard name
    #
    #    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    #         1    0.000    0.000    0.002    0.002 <string>:1(<module>)
    #        16    0.000    0.000    0.001    0.000 blit.py:130(return_word)
    #        16    0.000    0.000    0.000    0.000 blit.py:131(<listcomp>)
    #        16    0.000    0.000    0.000    0.000 blit.py:132(<listcomp>)
    #         3    0.000    0.000    0.000    0.000 blit.py:143(piramida_ease)
    #         1    0.000    0.000    0.002    0.002 blit.py:150(blit)
    #         3    0.000    0.000    0.000    0.000 blit.py:49(matrix_create)
    #         3    0.000    0.000    0.001    0.000 blit.py:69(piramida_create)
    #         3    0.000    0.000    0.000    0.000 blit.py:71(<listcomp>)
    #         3    0.000    0.000    0.000    0.000 blit.py:72(<listcomp>)
    #         3    0.000    0.000    0.000    0.000 blit.py:93(<listcomp>)
    #        16    0.000    0.000    0.000    0.000 blit.py:98(piramida_pattern)
    #         1    0.000    0.000    0.002    0.002 {built-in method builtins.exec}
    #        84    0.000    0.000    0.000    0.000 {built-in method builtins.format}
    #       408    0.000    0.000    0.000    0.000 {built-in method builtins.len}
    #        20    0.000    0.000    0.000    0.000 {built-in method builtins.print}
    #        98    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
    #        84    0.000    0.000    0.000    0.000 {method 'count' of 'list' objects}
    #       207    0.000    0.000    0.000    0.000 {method 'count' of 'str' objects}
    #         1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    #        37    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}



'''
Способ решение задачи с собеседования. Описание задачи в корне проекта.
Blit является своего рода алгоритмом контрольной суммы, который принимает двоичное число
 и вычисляет один бит контрольной суммы.

word_check(word) - проверка на корректность ввода мтроки пользователем
primary_create(len_word) - формируется начальная матрица индексов по длине слова
piramida_create (matrix, user_word) - создается матрица-шаблон, биты строки разбиваются для обработки
piramida_pattern(array_val) - преобразование значений по предложенным формулам
return_word(array_ind, array_val) - обратное преобразование для вывода пользователю
piramida_ease(array_val) - упрощение пирамиды, если все 1/0
blit(word) - основная ПП обработки
test() - проверка алгоритма на тестовых строках.
Строки для проверки работы алгоритма:
1110 1111 0011 1000
1100 1001 0000 1111
1101 1010 1010 0010
0010 0001 0110 1000
1100 0010 0011 0100
1110 1111 0011 1000 1100 1001 0000 1111 1101 1010 1010 0010 0010 0001 0110 1000

Произведена проверка времени работы с применением модулей timeit, cProfile

Вариант 2
C применением рекурсии.
'''

import re
from functools import reduce

# проверка на корректность ввода строки
def word_check(word):

    pattern = r'[a-zA-Zа-яёА-ЯЁ2-9_,\s]'

    check = re.search(pattern,word)

    if check == None:
        # проверка разрядности
        n, bit_depth = 1, 0
        while bit_depth < len(word):

            bit_depth = pow(4, n)
            n += 1

            if bit_depth > len(word):
                check = False
                print(f'Некорректная разрядность числа: {word}. Исправьте, и повторите попытку.')
                break

        check = True
    else:
        check = False
        print(f'Вы ввели некорректную строку: {word}. Исправьте, и повторите попытку.')

    return check

# создание первичной матрицы
def primary_create(len_word):
    # кол элементов строки пирамиды
    array_count, index = 0, 0
    array, row = [], []

    # формируем матрицу индексов
    for item in range(len_word):
        row.append(item)
        index += 1

        if index > array_count:
            array.append(row)
            row = []
            index = 0
            array_count += 2

    return array

# создание матрицы-индекс, матрицы-значение
def piramida_create(matrix, user_word):
    array_ind = [[-1 for _ in range(4)] for _ in range(int(len(user_word) / 4))]
    array_val = [[-1 for _ in range(4)] for _ in range(int(len(user_word) / 4))]

    row_index = 0
    row_full = None

    # перебор по значениям
    for line in matrix:
        if row_full != None:
            row_index = row_full

        for index in line:
            array_item = int(format(index, '5b')[-2:], 2)

            if (row_full != None) and (array_item < 2): row_index += 1
            array_ind[row_index][array_item] = index
            array_val[row_index][array_item] = user_word[index]

            # проверка строки массива на заполненность
            if not (array_ind[row_index].count(-1)):
                row_full = row_index

    array_result = reduce(lambda d, item: d.extend(item) or d,array_ind, [])

    val = [''.join(line) for line in array_val]

    return array_result, val

# преобразование матрицы по шаблонам
def piramida_pattern(array_val):
    # для удобства обработки, формат (последовательность бит) слова изменен 0-1-2-3 вместо 3-2-1-0
    patterns = {
        '0000': '0000',
        '1000': '0001',
        '0100': '1000',
        '1100': '0100',
        '0010': '0000',
        '1010': '0100',
        '0110': '1101',
        '1110': '1101',
        '0001': '0010',
        '1001': '1010',
        '0101': '1110',
        '1101': '1111',
        '0011': '1011',
        '1011': '0111',
        '0111': '1110',
        '1111': '1111'
    }

    # преобразуем по шаблонам
    result = list(map(lambda item: patterns[item], array_val))

    # проверка на законченность преобразований
    checksum = list(map(lambda item: len(set(item)),result))
    check = True if sum(checksum) == len(result) else False

    return result, check

# обратное преобразование для пользователя
def return_word(array_ind, array_val):
    array = ''.join(array_val)
    user_word = [None for _ in range(len(array_ind))]

    for index,item in enumerate(array_ind):
        user_word[item] = array[index]

    return ''.join(user_word)[::-1]

# упрощение слова
def piramida_ease(array_val):
    user_word = list(map(lambda item: item[0], array_val))
    return ''.join(user_word)[::-1]

def blit(word):

    # базовый случай
    if len(word) == 1:
        return word

    # преобразуем для работы
    user_word = word[::-1]

    # преобразование в первичную матрицу
    matrix = primary_create(len(user_word))

    # преобразование и разбиение на 4-разр. слова
    array_ind, array_val = piramida_create(matrix, user_word)

    # замена по шаблону
    is_check = False
    while is_check == False:
        array_val, is_check = piramida_pattern(array_val)
        # преобразование для вывода пользователю
        print(return_word(array_ind, array_val))

    # упрощение матрицы
    word = piramida_ease(array_val)
    print(word)

    word = blit(word)

    return word

# тест
def test():
    test_words = ['1110111100111000',
                  '1100100100001111',
                  '1101101010100010',
                  '0010000101101000',
                  '1100001000110100',
                  '1110111100111000110010010000111111011010101000100010000101101000'
                  ]
    for word in test_words:
        if word_check(word):
            print(f'Результат проверки слова {word}: {blit(word)}\n')
        else:
            print(f'Ошибка ввода слова {word}!\n')
            continue
    print('Проверка закончена')

if __name__ == '__main__':
    #word = input('Введите число для проверки:')
    #
    # проверка на корректность ввода значения -
    # if word_check(word):
    #     print(f'Результат проверки строки {word}: {blit(word)}')

    # проверка на тестовых строках
    test()

    # проверка на время выполнения
    #import timeit
    # python -m timeit -n 10000 -s "import blit" "blit.blit('1110111100111000')"
    # 1000 loops, best of 3: 995 usec per loop
    # 5000 loops, best of 3: 934 usec per loop
    # 10000 loops, best of 3: 990 usec per loop

    # python -m timeit -n 10000 -s "import blit" "blit.blit('1110111100111000110010010000111111011010101000100010000101101000')"
    # 1000 loops, best of 3: 2.51 msec per loop
    # 5000 loops, best of 3: 2.63 msec per loop
    # 10000 loops, best of 3: 2.82 msec per loop

    # import cProfile
    #
    # cProfile.run('blit("1110111100111000")')
    # 'blit("1110111100111000")'

    # 239 function calls (237 primitive calls) in 0.001 seconds
    #
    #    Ordered by: standard name
    #
    #    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    #         1    0.000    0.000    0.001    0.001 <string>:1(<module>)
    #        19    0.000    0.000    0.000    0.000 blite_v2.py:121(<lambda>)
    #        19    0.000    0.000    0.000    0.000 blite_v2.py:124(<lambda>)
    #         7    0.000    0.000    0.000    0.000 blite_v2.py:130(return_word)
    #         7    0.000    0.000    0.000    0.000 blite_v2.py:132(<listcomp>)
    #         2    0.000    0.000    0.000    0.000 blite_v2.py:140(piramida_ease)
    #         5    0.000    0.000    0.000    0.000 blite_v2.py:141(<lambda>)
    #       3/1    0.000    0.000    0.001    0.001 blite_v2.py:144(blit)
    #         2    0.000    0.000    0.000    0.000 blite_v2.py:50(primary_create)
    #         2    0.000    0.000    0.000    0.000 blite_v2.py:69(piramida_create)
    #         2    0.000    0.000    0.000    0.000 blite_v2.py:70(<listcomp>)
    #         2    0.000    0.000    0.000    0.000 blite_v2.py:71(<listcomp>)
    #         5    0.000    0.000    0.000    0.000 blite_v2.py:92(<lambda>)
    #         2    0.000    0.000    0.000    0.000 blite_v2.py:94(<listcomp>)
    #         7    0.000    0.000    0.000    0.000 blite_v2.py:99(piramida_pattern)
    #         2    0.000    0.000    0.000    0.000 {built-in method _functools.reduce}
    #         1    0.000    0.000    0.001    0.001 {built-in method builtins.exec}
    #        20    0.000    0.000    0.000    0.000 {built-in method builtins.format}
    #        42    0.000    0.000    0.000    0.000 {built-in method builtins.len}
    #         9    0.000    0.000    0.000    0.000 {built-in method builtins.print}
    #         7    0.000    0.000    0.000    0.000 {built-in method builtins.sum}
    #        26    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
    #        20    0.000    0.000    0.000    0.000 {method 'count' of 'list' objects}
    #         1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    #         5    0.000    0.000    0.000    0.000 {method 'extend' of 'list' objects}
    #        21    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}


    # 'blit("1110111100111000110010010000111111011010101000100010000101101000")'
    # 904 function calls (901 primitive calls) in 0.002 seconds
    #
    #    Ordered by: standard name
    #
    #    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    #         1    0.000    0.000    0.002    0.002 <string>:1(<module>)
    #       121    0.000    0.000    0.000    0.000 blite_v2.py:121(<lambda>)
    #       121    0.000    0.000    0.000    0.000 blite_v2.py:124(<lambda>)
    #        16    0.000    0.000    0.000    0.000 blite_v2.py:130(return_word)
    #        16    0.000    0.000    0.000    0.000 blite_v2.py:132(<listcomp>)
    #         3    0.000    0.000    0.000    0.000 blite_v2.py:140(piramida_ease)
    #        21    0.000    0.000    0.000    0.000 blite_v2.py:141(<lambda>)
    #       4/1    0.000    0.000    0.002    0.002 blite_v2.py:144(blit)
    #         3    0.000    0.000    0.000    0.000 blite_v2.py:50(primary_create)
    #         3    0.000    0.000    0.001    0.000 blite_v2.py:69(piramida_create)
    #         3    0.000    0.000    0.000    0.000 blite_v2.py:70(<listcomp>)
    #         3    0.000    0.000    0.000    0.000 blite_v2.py:71(<listcomp>)
    #        21    0.000    0.000    0.000    0.000 blite_v2.py:92(<lambda>)
    #         3    0.000    0.000    0.000    0.000 blite_v2.py:94(<listcomp>)
    #        16    0.000    0.000    0.001    0.000 blite_v2.py:99(piramida_pattern)
    #         3    0.000    0.000    0.000    0.000 {built-in method _functools.reduce}
    #         1    0.000    0.000    0.002    0.002 {built-in method builtins.exec}
    #        84    0.000    0.000    0.000    0.000 {built-in method builtins.format}
    #       166    0.000    0.000    0.000    0.000 {built-in method builtins.len}
    #        19    0.000    0.000    0.000    0.000 {built-in method builtins.print}
    #        16    0.000    0.000    0.000    0.000 {built-in method builtins.sum}
    #        98    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
    #        84    0.000    0.000    0.000    0.000 {method 'count' of 'list' objects}
    #         1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    #        21    0.000    0.000    0.000    0.000 {method 'extend' of 'list' objects}
    #        56    0.000    0.000    0.000    0.000 {method 'join' of 'str' objects}


'''
python 3.6
sqllite3 v.3.35.5
pandas 1.1.1
sqlalchemy 1.4.15

Задача:
1. Считать данные с kinopoisk.ru. За 1 день можно сделать не более 100 бесплатных запросов.
2. Сохранить в БД полученные данные
3. Обработка данных - вывод рейтинга фильмов

Результат запроса:
200 - Ок
400 - неверный формат запроса
403 - нет доступа
404 - путь не найден

Элементы для сохранения:
id
id_kinopoisk - номер
title - название (рус)
title_alternative - название (англ)
description - описание
year - год выпуска
age - возрастные ограничения
actors - актеры
countries - страна производитель
rating_kinopoisk - рейтинг кинопоиска
rating_imdb - рейтинг какой-то
premiere_world - премьера в мире
premiere_russia - премьера в России
'''
import requests
import  pandas as pd
import sqlite3
from sqlalchemy import create_engine, update, MetaData, Table, Column, Text, String, Integer
from sqlalchemy.exc import IntegrityError as Error

# получаем токен
def tokenread():
    with open('token.txt') as token_file:
        token = (token_file.read()).split('\n')

    return token

# запрос к источнику
def kontent_read(url, token):

    pages = list(range(1,2))
    print(pages)
    films_all = []

    spam = [
        {'id': 368539, 'id_kinopoisk': 1003588, 'url': '27832-gamilton-2015', 'type': 'movie', 'title': 'Гамильтон',
         'title_alternative': 'Hamilton', 'tagline': None, 'description': None, 'year': 2015,
         'poster': '//images.kinopoisk.cloud/posters/1003587.jpg',
         'trailer': 'https://www.youtube.com/embed/5snjgB9PO48', 'age': None,
         'actors': ['Лин-Мануэль Миранда','Лин-Мануэль Миранда','Лин-Мануэль Миранда'], 'countries': ['США'], 'genres': ['Мюзикл'], 'directors': ['Томас Каил'],
         'screenwriters': ['Лин-Мануэль Миранда', 'Ron Chernow'],
         'producers': ['Джилл Фурман', 'Sander Jacobs', 'Лин-Мануэль Миранда'], 'operators': None,
         'composers': ['Alex Lacamoire'], 'painters': ['Пол Тэйзуэлл'], 'editors': None, 'budget': None,
         'rating_kinopoisk': '9.261', 'rating_imdb': None, 'kinopoisk_votes': '510', 'imdb_votes': None,
         'fees_world': None, 'fees_russia': None, 'premiere_world': '6 августа 2015', 'premiere_russia': None,
         'frames': ['https://st.kp.yandex.net/im/kadr/2/8/2/kinopoisk.ru-Hamilton-2825620.jpg',
                    'https://st.kp.yandex.net/im/kadr/2/8/2/kinopoisk.ru-Hamilton-2825621.jpg'], 'screenshots': None,
         'videocdn': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'hdvb': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'collapse': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'kodik': {'url': None, 'quality': None, 'duration': None, 'voice': None}},
        {'id': 39841, 'id_kinopoisk': 45318, 'url': '408479-zhil-byl-pes-1982', 'type': 'movie', 'title': 'Жил-был пёс2',
         'title_alternative': None, 'tagline': None,
         'description': 'Жил-был пёс. Верно служил, но выгнали его по старости. И решил он повеситься, да повстречал в лесу такого же старого волка',
         'year': 1982, 'poster': '//images.kinopoisk.cloud/posters/45319.jpg',
         'trailer': 'https://www.youtube.com/embed/twSn58BPgWM', 'age': 'зрителям, достигшим 6 лет',
         'actors': ['Георгий Бурков','Георгий Бурков'], 'countries': ['СССР'],
         'genres': ['Мультфильм', 'Короткометражка', 'Комедия', 'Семейный'], 'directors': ['Эдуард Назаров'],
         'screenwriters': ['Эдуард Назаров'], 'producers': None, 'operators': ['Михаил Друян'], 'composers': None,
         'painters': ['Алла Горева', 'Эдуард Назаров'], 'editors': None, 'budget': None, 'rating_kinopoisk': '9.196',
         'rating_imdb': '8.6', 'kinopoisk_votes': '75750', 'imdb_votes': '2885', 'fees_world': None,
         'fees_russia': None, 'premiere_world': '12 июля 2016', 'premiere_russia': None,
         'frames': ['https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361251.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361252.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361253.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361254.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361255.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361256.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361257.jpg',
                    'https://st.kp.yandex.net/im/kadr/2/6/9/kinopoisk.ru-Zhil-byl-pyos-2698822.jpg',
                    'https://st.kp.yandex.net/im/kadr/2/6/9/kinopoisk.ru-Zhil-byl-pyos-2698823.jpg'],
         'screenshots': None, 'videocdn': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'hdvb': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'collapse': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'kodik': {'url': None, 'quality': None, 'duration': None, 'voice': None}},
        {'id': 39840, 'id_kinopoisk': 45319, 'url': '408479-zhil-byl-pes-1982', 'type': 'movie', 'title': 'Жил-был пёс4',
         'title_alternative': None, 'tagline': None,
         'description': 'Жил-был пёс. Верно служил, но выгнали его по старости. И решил он повеситься, да повстречал в лесу такого же старого волка',
         'year': 1982, 'poster': '//images.kinopoisk.cloud/posters/45319.jpg',
         'trailer': 'https://www.youtube.com/embed/twSn58BPgWM', 'age': 'зрителям, достигшим 6 лет',
         'actors': ['Георгий Бурков','Георгий Бурков'], 'countries': ['СССР'],
         'genres': ['Мультфильм', 'Короткометражка', 'Комедия', 'Семейный'], 'directors': ['Эдуард Назаров'],
         'screenwriters': ['Эдуард Назаров'], 'producers': None, 'operators': ['Михаил Друян'], 'composers': None,
         'painters': ['Алла Горева', 'Эдуард Назаров'], 'editors': None, 'budget': None, 'rating_kinopoisk': '9.196',
         'rating_imdb': '7', 'kinopoisk_votes': '75750', 'imdb_votes': '2885', 'fees_world': None,
         'fees_russia': None, 'premiere_world': '12 июля 2016', 'premiere_russia': None,
         'frames': ['https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361251.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361252.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361253.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361254.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361255.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361256.jpg',
                    'https://st.kp.yandex.net/im/kadr/3/6/1/kinopoisk.ru-Zhil-byl-pyos-361257.jpg',
                    'https://st.kp.yandex.net/im/kadr/2/6/9/kinopoisk.ru-Zhil-byl-pyos-2698822.jpg',
                    'https://st.kp.yandex.net/im/kadr/2/6/9/kinopoisk.ru-Zhil-byl-pyos-2698823.jpg'],
         'screenshots': None, 'videocdn': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'hdvb': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'collapse': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'kodik': {'url': None, 'quality': None, 'duration': None, 'voice': None}},
        {'id': 520468, 'id_kinopoisk': 326, 'url': '529004-pobeg-iz-shoushenka-1994', 'type': 'movie',
         'title': 'Побег из Шоушенка', 'title_alternative': 'The Shawshank Redemption',
         'tagline': '«Страх - это кандалы. Надежда - это свобода»',
         'description': 'Бухгалтер Энди Дюфрейн обвинён в убийстве собственной жены и её любовника. Оказавшись в тюрьме под названием Шоушенк, он сталкивается с жестокостью и беззаконием, царящими по обе стороны решётки. Каждый, кто попадает в эти стены, становится их рабом до конца жизни. Но Энди, обладающий живым умом и доброй душой, находит подход как к заключённым, так и к охранникам, добиваясь их особого к себе расположения.',
         'year': 1994, 'poster': '//images.kinopoisk.cloud/posters/326.jpg',
         'trailer': 'https://www.youtube.com/embed/m_0oN0jgsg8', 'age': 'зрителям, достигшим 16 лет',
         'actors': ['Тим Роббинс','Георгий Бурков'],
         'countries': ['США'], 'genres': ['Драма'], 'directors': ['Фрэнк Дарабонт'],
         'screenwriters': ['Фрэнк Дарабонт', 'Стивен Кинг'],
         'producers': ['Лиз Глоцер', 'Дэвид В. Лестер', 'Ники Марвин'], 'operators': ['Роджер Дикинс'],
         'composers': ['Томас Ньюман'], 'painters': ['Теренс Марш', 'Soheil', 'Питер Лэндсдаун Смит'],
         'editors': ['Ричард Фрэнсис-Брюс'], 'budget': '$25000000', 'rating_kinopoisk': '9.1', 'rating_imdb': '7',
         'kinopoisk_votes': None, 'imdb_votes': None, 'fees_world': '$28418687', 'fees_russia': '$87432',
         'premiere_world': '10 сентября 1994', 'premiere_russia': '24 октября 2019',
         'frames': ['https://st.kp.yandex.net/im/kadr/4/4/8/kinopoisk.ru-The-Shawshank-Redemption-44864.jpg', ],
         'videocdn': {'url': ['/movie/b00eabf79a13cf75ebbfde77257b7437/iframe'], 'quality': ['HDRip'], 'duration': None,
                      'voice': ['Дубляж']}, 'hdvb': {'url': None, 'quality': None, 'duration': None, 'voice': None},
         'collapse': {'url': ['/embed/movie/162'], 'quality': ['HD (720p)'], 'duration': ['142 мин. / 02:22'],
                      'voice': ['Рус. Проф. многоголосый', 'Eng.Original']},
         'kodik': {'url': None, 'quality': None, 'duration': None, 'voice': None}},
         ]
    for page in pages:
        source_url = f'https://{url}/movies/all/page/{page}/token/{token}'

        # запрос
        # response = requests.get(source_url, params={})
        # print(response)
        #
        # # информация в виде json
        # films_info = response.json()
        # print(films_info['movies'])
        #
        # # готовим данные
        # for item in films_info['movies']:
        #     if isinstance(item['actors'], list):
        #         item['actors'] = ','.join(item['actors'])
        #     if isinstance(item['countries'], list):
        #         item['countries'] = ','.join(item['countries'])
        #
        # # объединяем
        # films_all.extend(films_info['movies'])

        for item in spam:
            if isinstance(item['actors'], list):
                item['actors'] = ','.join(item['actors'])

            if isinstance(item['countries'], list):
                item['countries'] = ','.join(item['countries'])

        films_all.extend(spam)


    # организуем датафрайм
    df = pd.DataFrame(films_all, columns=[
        'id',
        'id_kinopoisk',
        'title',
        'title_alternative',
        'year',
        'age',
        'actors',
        'countries',
        'rating_kinopoisk',
        'rating_imdb',
        'premiere_world',
        'premiere_russia'])

    return df

# def connect_db():
#     path = 'c:\SQLLite\movieDB\movies.db'
#
#     # подключение к БД
#     engine = sqlite3.connect(path)
#     cur = engine.cursor()
#
#     return engine, cur

# подключение к БД
def connect_alch_db():
    path = 'c:\SQLLite\movieDB\movies.db'
    engine = create_engine('sqlite:///' + path)

    return engine

# проверка данных перед записью в БД
def checkdata(df, engine):
    # забираем данные из БД
    df_exist = pd.read_sql('movies', con = engine)

    # объединяем
    df_unity_id = pd.concat([df['id_kinopoisk'],df_exist['id_kinopoisk']])

    # получаем список дубликатов
    isexist_id = df_unity_id[df_unity_id.duplicated(keep=False)].drop_duplicates(keep='first').tolist()
    print(f'********\n {isexist_id}')

    df_new = df.loc[~df['id_kinopoisk'].isin(isexist_id)]
    df_update = df.loc[df['id_kinopoisk'].isin(isexist_id)]
    print(f'**new** \n{df_new}')
    print(f'**update** \n{df_update}')

    return df_new, df_update

# создание метаданных
def create_table(engine):
    # создаем класс, и табл. в нем
    md = MetaData()

    # основная
    movies = Table('movies', md,
                   Column('id', Integer(), nullable=False),
                   Column('id_kinopoisk', Integer(), nullable=False),
                   Column('title',String(), nullable=False),
                   Column('title_alternative', String(), nullable=False),
                   Column('year', Integer(), nullable=False),
                   Column('age', String(), nullable=False),
                   Column('actors', Text(), nullable=False),
                   Column('countries', String(), nullable=False),
                   Column('rating_kinopoisk', String(), nullable=False),
                   Column('rating_imdb', String(), nullable=False),
                   Column('premiere_world',String(), nullable=False),
                   Column('premiere_russia', String(), nullable=False)
                   )

    # временная
    # cols = [c.copy() for c in movies.columns]
    # constraints = [c.copy() for c in movies.constraints]
    # mirror = Table('movies_mirror', md, *(cols + constraints))  #,prefixes=['TEMPORARY']

    md.create_all(engine)

    # проверка
    for t in md.tables:
        print(md.tables[t])

    sqlcon = engine.connect()

    return sqlcon, movies#, mirror

# добавление данных в БД
def db_add(engine, df_new):
    df_new.to_sql('movies', con=engine,if_exists='append',index=False, method='multi')

def db_update(sqlcon, movies, df_update):

    result = 0
    # рабочий вариант для n - строк
    for i in range(len(df_update)):
        # dbrequest = update(movies).where(movies.c.id_kinopoisk == int(df_update.id_kinopoisk.iloc[i])). \
        #     values(rating_kinopoisk=df_update.rating_kinopoisk.iloc[i])
        dbrequest = update(movies).where(movies.c.id_kinopoisk == int(df_update.id_kinopoisk.iloc[i])). \
            values({movies.c.rating_kinopoisk:df_update.rating_kinopoisk.iloc[i],
                    movies.c.rating_imdb:df_update.rating_imdb.iloc[i]})
        try:
            dbresult = sqlcon.execute(dbrequest)
        except:
            # сообщение об ошибке
            print('Ошибка при обращении к БД')
            sqlcon.rollback()

        # кол. записанных строк
        result += dbresult.rowcount
        print(f'id_kinopoisk={df_update.id_kinopoisk.iloc[i]}')

    return result

# def temp():
#     films_all = []
#     tmp = [{'id': 123456}, {'id': 25689}, {'id': 123753}, {'id':45687239}]
#     pages = list(range(1, 4))
#     for page in pages:
#         films_all.extend(tmp)
#
#     return films_all

if __name__ == '__main__':

    # подключене/считывание данных из источника
    source = tokenread()
    df = kontent_read(source[0],source[1])
    print(df)

    engine = connect_alch_db()

    # добавление данных в БД
    #df.to_sql('movies', con=engine,if_exists='append',index=False, method='multi')

    df_new, df_update = checkdata(df, engine)

    sqlcon, movies = create_table(engine)
    print(f'sqlcon = {sqlcon}')
    print(f'movies = {movies}')

    result = db_update(sqlcon, movies, df_update)
    print(f'количество обновленных записей : {result}')
'''
Постановка задачи:
Разработать алгиртм, позволяющий найти к-е число из упорядоченного числового ряда, в разложении элементов которого
на простые множители присутстуют только 3, 5 и 7.
'''

def find_k(k):
    # начальные данные
    mults = [3, 5, 7]
    order_row = [1]
    setmults = set()

    # условие выхода из цикла
    while len(order_row) < k:
        if (len(order_row) + len(setmults)) < k:
            for mult in mults:
                setmults.add(mult * order_row[-1])

        order_row.append(min(setmults))
        setmults.remove(min(setmults))

    return order_row

if __name__ == '__main__':
    k = int(input('Введите номер элемента числового ряда, для поиска: '))
    order_row = find_k(k=k)
    print(f'В результате получаем числовой ряд:\n {order_row}')
    print(f'k-й элемент числового рядя: {order_row[-1]}')
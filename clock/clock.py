'''
Задача:  Написать код, который будет рассчитывать угол между часовой и минутной стрелкой в заданное время.
Допущение : т.к. в результате получается 2 угла, то для результата выбираем - по часовой стрелке.

'''

# проверка ввода пользователя
def check_time(time):

    dtime = time.split(':')
    assert len(dtime) == 2, 'Ошибка: формат ввода времени "hh:mm"!'
    assert dtime[0].isdigit() and dtime[1].isdigit(), 'Ошибка: часы, минуты введены не корректно!'
    assert int(dtime[0]) < 24, 'Ошибка: часы введены не корректно!'
    assert int(dtime[1]) < 60, 'Ошибка: минуты введены не корректно!'

    return True

# расчет угла
def angle(time):

    d_angle_h = 360 / 12    # угол часового сегмента
    d_angle_mm = 360 / 60   # угол мин. сегмента
    d_angle_hmm = d_angle_h / 60    # приращение угла (по мин.) в часовом сегменте

    dtime = time.split(':')
    hh, mm = int(dtime[0]), int(dtime[1])

    hh = hh - 12 if hh >= 12 else hh

    result_angle = abs((hh * d_angle_h + mm * d_angle_hmm) - mm * d_angle_mm)

    return result_angle

def test():
    # Значения углов расчитаны на онлайн калькуляторе abakbot.ru
    times = ['15:45', '19:05', '17:43', '16:15', '13:13', '11:25', '20:00', '9:01 ','2:00','11:10', '10:17', '9:03 ']
    angles = [157.5, 182.5, 86.5, 37.5, 41.5, 192.5, 240, 264.5, 60, 275, 206.5, 253.5]
    for index, item in enumerate(times):
        assert angle(item) == angles[index], f"Ошибка расчета угла для значения времени {item}"
        print(f'Для времени {item} угол составляет {angle(item)} градусов')

    return ('Проверка прошла успешно')

if __name__ == '__main__':

    time = input('Введите время в формате "hh:mm" : ')
    print(angle(time)) if check_time(time) else print('Ошибка ввода')

    #test()
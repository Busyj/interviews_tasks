'''
    Тест- программа. Проверка работы комманд по чтению списка открытых приложений из "Диспетчера задач" ОС.
    И закрытия приложений.
'''
import subprocess
import pprint
import os

from modules.dbase_lib import DB_shelve_create, UserDB
from modules.schedule_lib import Schedules
import modules.mlogger as mlogger

log = mlogger.get_logger(__name__)


def task_read():
    # чтение списка открытых программ
    pid = [line.decode('cp866', 'ignore').replace('"', '') for line in
           subprocess.check_output("tasklist /FO CSV").splitlines()]
    pprint.pprint(pid)
    print('*' * 10)
    print(f"пример вывода - {pid[5].split(',')}")

    # закрытие приложения c pid 7672
    subprocess.Popen(f"taskkill -f -t -pid {2828}")
    # subprocess.Popen(f"taskkill /PID {2828}")


def adapter_scaner():
    '''
    С помощью команды netsh получаем список сетевых соединений, и их текущий статус
    '''

    links = [line.decode('cp866', 'ignore') for line in
           subprocess.check_output('netsh interface show interface').splitlines()]
    pprint.pprint(links)

    user_adapters = []
    for i,line in enumerate(links):
        if i > 2 and len(line) > 0:
            user_adapter = {}
            adapter_property = line.split()
            print(f'{i} ) data = {adapter_property}')
            adapter_state = 1 if adapter_property[1] == 'Connected' or adapter_property[1] == 'Подключен' else 0
            adapter_name = ' '.join(adapter_property[3:])
            #print(f'{adapter_name}  sost = {adapter_sost}')
            user_adapters.append([adapter_name, adapter_state])

    print(user_adapters)
    return user_adapters


def adapter_disabled(name_interface):
    # Перед командой, проверка сост. подключения обязательна!
    # откл-е сетевого соединения, команда netsh. 200 - успешно, 400 - ошибка выполения команды
    res_disable = subprocess.check_output(f'netsh interface set interface "{name_interface}" disabled')
    status = 200 if res_disable == b'\r\n' else 400
    return status


def adapter_enabled(name_interface):
    # Перед командой, проверка сост. подключения обязательна!
    # подкл. сетевого соединения, команда netsh. 200 - успешно, 400 - ошибка выполения команды
    res_enable = subprocess.check_output(f'netsh interface set interface "{name_interface}" enabled')
    status = 200 if res_enable == b'\r\n' else 400
    return status


def userdb_run() -> UserDB:
    # создание БД
    db_element = DB_shelve_create()
    db = UserDB(db_element=db_element)
    db.user_db_make()
    db.attach()
    return db


def userdb_test() -> Schedules :
    # подключение к БД
    userdb = userdb_run()
    username = 'User_test'

    # Определяем класс
    user_schedule = Schedules(userdb=userdb,username=username)

    return user_schedule


def new_user_schedule(user_schedule: Schedules):
    '''Запись нового расписния в БД'''
    new_schedule = user_schedule.schedules_test()
    print(f'новое расписание {new_schedule}')
    user_schedule.set_clock(new_schedule)
    user_schedule.set_db_schedule_config()
    user_schedule.get_db_schedule_config()
    user_schedule.handler_now_schedule()


def get_username():
    '''
    получаем имя пользователя
    '''
    username = os.environ.get('USERNAME')
    print(f'Вариант 1 : {username}')
    username = os.getlogin()
    print(f'Вариант 2 : {username}')
    username = os.path.expanduser('~')
    print(f'Вариант 3 : {username}')

def main():

    # чтение списка задач и закрытие приложения
    # task_read()

    # вкл/откл сетевого адаптера
    #user_adapters = adapter_scaner()

    #name_interface = user_adapters[3][0]
    #cmd_status = adapter_enabled(name_interface=name_interface)
    #print(cmd_status)

    # подкл. БД
    #!user_schedules = userdb_test()
    # запись нового расписания
    #new_user_schedule(user_schedules)
    #!print(f'состояние - {user_schedules.inspan}')

    get_username()



if __name__ == '__main__':
    main()


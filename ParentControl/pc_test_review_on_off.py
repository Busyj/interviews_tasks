'''
    Тест- программа.
    Запуск\ останов проверки расписания.
'''
import subprocess
import pprint
import os
from sys import argv



from modules.dbase_lib import DB_shelve_create, UserDB
from modules.schedule_lib import Schedules


def userdb_run() -> UserDB:
    # создание БД
    db_element = DB_shelve_create()
    db = UserDB(db_element=db_element)
    db.user_db_make()
    db.attach()
    return db


def userdb_test() -> Schedules :
    # подключение к БД
    userdb = userdb_run()
    username = 'User_test'

    # Определяем класс
    user_schedule = Schedules(userdb=userdb,username=username)

    return user_schedule


def main():

    # подкл. БД
    user_schedules = userdb_test()
    print(argv)
    print(f'Текущее состояние проверки- {user_schedules.review_state}')
    state = argv[1].lower()
    if state == 'true':
        user_schedules.set_db_review_state(True)
    elif state == 'false':
        user_schedules.set_db_review_state(False)
    else:
        print('введен неверный ключ')


if __name__ == '__main__':
    main()


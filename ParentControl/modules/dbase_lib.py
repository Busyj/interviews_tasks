'''
Модуль запросов к БД.
Создание классов с помощью абстрактной фабрики.
'''

from abc import abstractmethod, ABC
import shelve
import os

import modules.mlogger as mlogger

log = mlogger.get_logger(__name__)


# абстр. класс создания БД
class DB_default_create(ABC):
    @abstractmethod
    def create_database(self):
        pass


# абстр.класс взаимодействия с БД
class DBdefault(ABC):
    @abstractmethod
    def db_attach(self):
        pass

    @abstractmethod
    def db_connect(self):
        pass

    @abstractmethod
    def db_insert(self, request, data):
        pass

    @abstractmethod
    def db_select(self, request):
        pass

    @abstractmethod
    def db_disconnect(self):
        pass


# работа с БД в shelve
class DB_shelve(DBdefault):
    def __init__(self):
        self.apppath = os.getcwd()
        log.info(f'Путь к директории проекта: {self.apppath}')


    def db_attach(self):
        self.dbpath = self.apppath + '\database\db_schedule'
        #!TODO проверка на ошибки при подключении. Обработка ошибок
        log.info(f'Путь к файлу Хранилища определен: {self.dbpath}')


    def db_connect(self) -> bool:
        try:
            self.db = shelve.open(self.dbpath)
            log.info('Соединение с Хранилищем - Успешно')
            status = True
        except:
            log.exception('Соединение с Хранилищем -Ошибка!')
            status = False

        return status


    def db_insert(self, request, data):
        try:
            self.db[request] = data
            log.info('Добавление в Хранилище данных - Успешно')
            status = True
        except:
            log.exception('Добавление в Хранилище данных - Ошибка!')
            status = False

        return status


    def db_select(self, request):
        response = ''
        try:
            response = self.db[request]
            log.info('Выборка из Хранилища данных')
        except:
            log.exception('Выборка из Хранилище данных - Ошибка!')

        return response


    def db_disconnect(self):
        try:
            self.db.close()
            log.info('Отключение от Хранилища')
        except:
            log.exception('Отключение от Хранилище - Ошибка!')


# фабрика создания класса db_shelve
class DB_shelve_create(DB_default_create):
    def create_database(self):
        return DB_shelve()


class UserDB:
    def __init__(self, db_element: DB_default_create):
        self.db_element = db_element

    def user_db_make(self):
        self.userdb = self.db_element.create_database()

    def attach(self):
        self.userdb.db_attach()

    def connect(self):
        self.userdb.db_connect()

    def insert(self, request, data):
        self.userdb.db_insert(request, data)

    def select(self, request):
        response = self.userdb.db_select(request)
        return response

    def disconnect(self):
        self.userdb.db_disconnect()


if __name__ == '__main__':
    db_element = DB_shelve_create()
    db = UserDB(db_element=db_element)
    db.user_db_make()
    db.attach()
    db.connect()

    dbpath = os.getcwd() + '\db_schedule'
    db.insert('path', dbpath)

    print(db.select('path'))

    db.disconnect()

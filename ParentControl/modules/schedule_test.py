'''

Отработка алгоритмов работы с расписанием
Формат расписания по часам, по дням недели: [[[8:30,9:00],[12:30, 18:00]],[вторник],[среда],[],[],[],[восскресенье]]

'''

# !todo создать класс с миним. настройками, запись параметров в БД и чтение.
# !todo создать работу с БД.

from time import time, localtime, struct_time, mktime, strptime
from ParentControl.modules.dbase_lib import DB_shelve_create, UserDB
import os

def userdb_run():
    db_element = DB_shelve_create()
    db = UserDB(db_element=db_element)
    db.user_db_make()
    db.attach()
    db.connect()
    dbpath = os.getcwd() + '\db_schedule'
    db.insert('path', dbpath)

    print(db.select('path'))

    db.disconnect()


def get_now_data():
    '''
        Получение текущей даты для дальнейших расчетов.
        Вывод в формате: struct_time
    '''
    now_sec = time()
    now_struct_time = localtime(now_sec)
    print(f'time: {now_struct_time.tm_hour}:{now_struct_time.tm_min}, week - {now_struct_time.tm_wday}')

    # return [now_struct_time.tm_wday, now_struct_time.tm_hour, now_struct_time.tm_min]
    return now_struct_time


def get_now_data_for_user():
    '''
            Получение текущей даты для дальнейших расчетов.
            Вывод в формате: ["<день недели>", "часы : мин"]
    '''
    wdays = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']

    now_sec = time()
    now_struct_time = localtime(now_sec)
    print(f'time: {now_struct_time.tm_hour}:{now_struct_time.tm_min}, week - {wdays[now_struct_time.tm_wday]}')

    return [wdays[now_struct_time.tm_wday], f'{now_struct_time.tm_hour}:{now_struct_time.tm_min}']


def get_now_schedule(wday: int):
    '''
        Получение расписания на текущей день, для дальнейшей работы.
    '''
    # !todo здесь должен быть запрос к БД или к др. ситочнику

    schedules = [[], [], [], [], [],
                 [['17:30', '19:00'], ['20:00', '21:00']],
                 [['14:30', '19:00'], ['20:00', '21:00']]
                 ]

    now_schedule = schedules[wday] if len(schedules[wday]) != 0 else [['00:00', '23:59']]

    return now_schedule


def isinspan_schedule(now_time: struct_time, now_schedules: list):
    '''
        Проверка: входит текущее время в разрешенный диапазон или нет. На выходе True/False.
        Алгоритм: Каждый элемент расписания пропускаем через функцию - to_localsec.
        Она преобразует их в локальные секунды от начала эпохи. Эти значения сравниваем с текущими лок.сек.
    '''
    print(now_time)
    print(now_schedules)

    def to_localsec(now_time, elem_schedule):
        format_time = '%d.%m.%Y %H:%M:%S'
        hm_new = f'{now_time.tm_mday}.{now_time.tm_mon}.{now_time.tm_year} {elem_schedule}:00'
        local_sec = mktime(strptime(hm_new, '%d.%m.%Y %H:%M:%S'))
        return local_sec

    hm_now = mktime(now_time)
    print(hm_now)

    for schedule in now_schedules:
        hm_beg = to_localsec(now_time, schedule[0])
        hm_end = to_localsec(now_time, schedule[1])
        print(f'{hm_beg}, {hm_end}')

        if hm_beg < hm_now < hm_end:
            return True
    return False


def end_usertime():
    '''
        Запускает процедуру в случае,  если текущее время не входит в диапазоны расписания.
    '''
    pass


def main():
    # получаем параметры текущей даты
    now_data_param = get_now_data()
    print(get_now_data_for_user())

    index_wday = now_data_param.tm_wday
    now_schedule = get_now_schedule(index_wday)

    print(isinspan_schedule(now_data_param, now_schedule))
    print('-' * 20)

    userdb_run()



if __name__ == '__main__':
    main()

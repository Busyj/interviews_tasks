'''
Модуль для работы с логгером.
Создание и ведение журнала ошибок, сообщений и т.д.

'''

import logging
import os

# [%(color)s] -
_log_format = f'%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s'


class CustomFilter(logging.Filter):
    # цвет записей в журнале
    COLOR = {
        'DEBUG': 'GREEN',
        'INFO': 'GREEN',
        'WARNING': 'YELLOW',
        'ERROR': 'RED',
        'CRITICAL': 'RED',
    }

    def filter(self, record):
        record.color = CustomFilter.COLOR[record.levelname]
        return True


def add_color_to_emit_ansi(action):

    def new(*args):
        print(args)
        levelno = args[0].levelno
        if levelno >= 50:
            color = '\x1b[31m'
        elif levelno >= 40:
            color = '\x1b[31m'
        elif levelno >= 30:
            color = '\x1b[33m'
        elif levelno >= 20:
            color = '\x1b[34m'
        elif levelno >= 10:
            color = '\x1b[34m'
        else:
            color = '\x1b[0m'

        args[0].levelname = color + args[0].levelname + '\x1b[0m'
        # args[0].filename = color + args[0].filename + '\x1b[0m'
        # args[0].funcName = color + args[0].funcName + '\x1b[0m'
        # args[0].name = color + args[0].name + '\x1b[0m'
        args[0].msg = color + args[0].msg + '\x1b[0m'
        return action(*args)
    return new


def get_file_handler():
    logpath = f'{os.getcwd()}\\logs\\debug.log'
    file_handler = logging.FileHandler(logpath,'w')
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(logging.Formatter(_log_format))
    return file_handler


def get_stream_handler():
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)

    stream_handler.setFormatter(logging.Formatter(_log_format))
    stream_handler.emit = add_color_to_emit_ansi(stream_handler.emit)
    return stream_handler


def get_logger(logname):
    # описываем экземпляр логгера (имя, уровень логирования, файл журнала, форматировние сообщений
    logger = logging.getLogger(logname)
    logger.setLevel(logging.INFO)
    #logger.addFilter(CustomFilter())
    logger.addHandler(get_file_handler())
    logger.addHandler(get_stream_handler())
    return  logger


if __name__ == '__main__':
    get_file_handler()
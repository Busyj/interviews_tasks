'''
Модуль для работы с расписанием.
Создание класса Schedule() и его функций
    self.userdb     # БД
    self.type       # тип расписания
    self.counters   # список по времени
    self.clockers   # расписание по часам
    self.counter_pc # счетчик работы ПК
    self.counter_web # счетчик работы в интернете
    self.counter_timer # период обновления счетчиков

'''
import threading
from time import time, localtime, struct_time, mktime, strptime
from modules.dbase_lib import DB_shelve_create, UserDB
import modules.mlogger as mlogger

log = mlogger.get_logger(__name__)


class Schedules():
    def  __init__(self, userdb: UserDB, username: str):
        self.userdb = userdb    # БД
        self.username = username
        self._schedule_config_default()
        self._get_db_schedule_config()
        self.review_schedule_start()


    def _schedule_config_default(self):
        self.type = 2
        self.counters = []
        self.clockers = []
        self.counter_pc = 0
        self.counter_web = 0
        self.counter_timer = 5
        self.inspan = False
        self.review_state = False
        log.info('Инициализация экземпляра "Расписание" ')


    def _set_db_schedule_config(self):

        config = dict(
            type = self.type,
            counters = self.counters,
            clockers = self.clockers,
            counter_pc = self.counter_pc,
            counter_web = self.counter_web,
            counter_timer = self.counter_timer
        )

        self.userdb.connect()
        status = self.userdb.insert(self.username, config)
        self.userdb.disconnect()
        log.info('Запись конфигурации в БД')

        return status


    def _get_db_schedule_config(self):
        self.userdb.connect()
        config = self.userdb.select(self.username)
        self.userdb.disconnect()

        self.type = config['type']
        self.counters = config['counters']
        self.clockers = config['clockers']
        self.counter_pc = config['counter_pc']
        self.counter_web = config['counter_web']
        self.counter_timer = config['counter_timer']

        log.info(f'Чтение конфигурации из БД:\n{config}')


    def set_db_review_state(self,review_state):
        state = dict(review_state = review_state)
        self.userdb.connect()
        status = self.userdb.insert('state', state)
        self.userdb.disconnect()

        log.info(f'Задать состояние проверки в БД, вкл.:{state}')
        return status


    def get_db_review_state(self):
        self.userdb.connect()
        state = self.userdb.select('state')
        self.userdb.disconnect()

        self.review_state = state['review_state']
        log.info(f'Считано из БД состояние проверки, вкл.:{state}')


    def get_now_data(self):
        '''
            Получение текущей даты для дальнейших расчетов.
            Вывод в формате: struct_time
        '''
        now_sec = time()
        now_struct_time = localtime(now_sec)
        log.info(f'Текущая дата:\ntime: {now_struct_time.tm_hour}:{now_struct_time.tm_min}, week - {now_struct_time.tm_wday}')

        # return [now_struct_time.tm_wday, now_struct_time.tm_hour, now_struct_time.tm_min]
        return now_struct_time


    def get_now_data_for_user(self):
        '''
                Получение текущей даты для дальнейших расчетов.
                Вывод в формате: ["<день недели>", "часы : мин"]
        '''
        wdays = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']

        now_sec = time()
        now_struct_time = localtime(now_sec)

        return [wdays[now_struct_time.tm_wday], f'{now_struct_time.tm_hour}:{now_struct_time.tm_min}']


    def get_now_schedule(self, wday: int):
        '''
            Получение расписания на текущей день, для дальнейшей работы.
        '''
        now_schedule = self.clockers[wday] if len(self.clockers[wday]) != 0 else [['00:00', '23:59']]

        return now_schedule


    def isinspan_schedule(self, now_time: struct_time, now_schedules: list):
        '''
            Проверка: входит текущее время в разрешенный диапазон или нет. На выходе True/False.
            Алгоритм: Каждый элемент расписания пропускаем через функцию - to_localsec.
            Она преобразует их в локальные секунды от начала эпохи. Эти значения сравниваем с текущими лок.сек.
        '''
        log.info(f'{now_time}')
        log.info(f'{now_schedules}')

        def to_localsec(now_time, elem_schedule):
            format_time = '%d.%m.%Y %H:%M:%S'
            hm_new = f'{now_time.tm_mday}.{now_time.tm_mon}.{now_time.tm_year} {elem_schedule}:00'
            local_sec = mktime(strptime(hm_new, format_time))
            return local_sec

        hm_now = mktime(now_time)
        log.info(f'{hm_now}')

        for schedule in now_schedules:
            hm_beg = to_localsec(now_time, schedule[0])
            hm_end = to_localsec(now_time, schedule[1])
            log.info(f'{hm_beg}, {hm_end}')

            if hm_beg < hm_now < hm_end:
                return True
        return False


    def handler_now_schedule(self):
        '''
            Проверка вхождения текущего времени в разрешенный диапазон расписания
        '''

        # получаем параметры текущей даты
        now_data_param = self.get_now_data()

        # получаем день недели и расчет текущего расписания
        index_wday = now_data_param.tm_wday
        self.now_schedule = self.get_now_schedule(index_wday)

        # вхождение в диапазон
        self.inspan = self.isinspan_schedule(now_data_param, self.now_schedule)

        #return isinspan


    def review_schedule_start(self):
        '''
        запуск проверки по расписанию в потоке с интервалом self.counter_timer
        '''

        self.counter_timer = 5
        self.handler_now_schedule()
        # self.counter_pc += 1
        # if self.counter_pc <= 10:

        self.get_db_review_state()

        if self.review_state:
            self.review_threading = threading.Timer(self.counter_timer,self.review_schedule_start).start()


    def set_clock(self, clockers):
        ''' Задаем новое расписание на дни недели'''
        self.clockers = clockers


    def end_usertime(self):
        '''
            Запускает процедуру в случае,  если текущее время не входит в диапазоны расписания.
        '''
        pass


    def schedules_test(self):
        clockers = [
            [['17:30', '19:15']], [], [], [], [],
            [['17:30', '19:00'], ['20:00', '21:00']],
            [['14:20', '19:00'], ['20:00', '21:00']]
                     ]
        return clockers


def userdb_run()-> UserDB:
    db_element = DB_shelve_create()
    db = UserDB(db_element=db_element)
    db.user_db_make()
    db.attach()
    return db


if __name__ == '__main__':

    #!TODO 1. вывести из класса Расписание взаимодействие с Хранилищем (нарушает SOLID). Огранизовать отдельным функционалом.
    #!TODO 2. определить счетчик времени. Когда счетчик запущен, сколько времени осталось до заданного.
    #!TODO Получить счетчик текущего дня.
    #!TODO 3. Перевести внутр. функции и аргументы в недоступный для внеш. запроса режим.
    # !TODO 4. Предусмотреть определение времени через сеть (часовой пояс, геолокация).
    #!TODO  Или настройка синхронизации времени ПК через сервер времени в сети.

    # подключение к БД
    userdb = userdb_run()
    username = 'User_test'

    # Определяем класс
    user_schedule = Schedules(userdb=userdb,username=username)
    user_schedule.schedules_test()
    user_schedule.get_db_schedule_config()

    # # получаем параметры текущей даты
    # now_data_param = user_schedule.get_now_data()
    # print(user_schedule.get_now_data_for_user())
    #
    # index_wday = now_data_param.tm_wday
    # now_schedule = user_schedule.get_now_schedule(index_wday)
    #
    # print(user_schedule.isinspan_schedule(now_data_param, now_schedule))
    # print('-' * 20)


'''
Модуль работы с сетевым интерфейсом.
Описание класса Adapters() со всеми функциями.
'''

import subprocess
import pprint
import os

import modules.mlogger as mlogger

log = mlogger.get_logger(__name__)

class Adapters():
    def __init__(self):
        self.revision_table = []

    def adapter_scaner(self):
        '''
        С помощью команды netsh получаем список сетевых соединений, и их текущий статус
        '''

        links = [line.decode('cp866', 'ignore') for line in
                 subprocess.check_output('netsh interface show interface').splitlines()]
        pprint.pprint(links)

        user_adapters = []
        for i, line in enumerate(links):
            if i > 2 and len(line) > 0:
                adapter_property = line.split()
                adapter_state = 1 if adapter_property[1] == 'Connected' or adapter_property[1] == 'Подключен' else 0
                adapter_name = ' '.join(adapter_property[3:])
                user_adapters.append([adapter_name, adapter_state])

        log.info(user_adapters)
        return user_adapters


    def adapter_disabled(self, name_interface:str, state_interface: int):
        # Перед командой, проверка сост. подключения обязательна!

        status = 0
        if state_interface:
            # откл-е сетевого соединения, команда netsh. 200 - успешно, 400 - ошибка выполения команды
            res_disable = subprocess.check_output(f'netsh interface set interface "{name_interface}" disabled')
            status = 200 if res_disable == b'\r\n' else 400

            msg = 'отключено' if status == 200 else '-ошибка отключения'
            log.info(f'Cетевое соединение "{name_interface}" {msg}')

        return status


    def adapter_enabled(self, name_interface: str, state_interface: int):
        # Перед командой, проверка сост. подключения обязательна!

        status = 0
        if not state_interface:
            # подкл. сетевого соединения, команда netsh. 200 - успешно, 400 - ошибка выполения команды
            res_enable = subprocess.check_output(f'netsh interface set interface "{name_interface}" enabled')
            status = 200 if res_enable == b'\r\n' else 400

            msg = 'подключено' if status == 200 else '-ошибка подключения'
            log.info(f'Cетевое соединение "{name_interface}" {msg}')

        return status

    #!todo 1. ввести список сетей для мониторинга. 2.Проверить проверяемых состояние сетей перед откл\подкл.
    #!todo 3. проход по всем сетям из списка с командой пользоваетля.
    #!TODO 4. Определяем учетную запись пользователя. Полуаем ее уровень доступа.
    #!TODO 5. Авторизация.
    #!TODO 6. Запуск приложения как службу ОС.

    def set_revision_table(self, table):
        # задаем таблицу отслеживаемых сетевых соединений

        self.revision_table = table
        return self.revision_table


def main():
    # Определяем класс
    user_adapters = Adapters()

    # вкл/откл сетевого адаптера
    table_adapters = user_adapters.adapter_scaner()


if __name__ == '__main__':
    main()
